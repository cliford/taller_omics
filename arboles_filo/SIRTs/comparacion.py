import sys
import re

# Verificar que se han proporcionado los nombres de los archivos de entrada
if len(sys.argv) < 3:
    print('Uso: python script.py <archivo1> <archivo2>')
    sys.exit(1)

print('Iniciando el proceso de comparación de especies...')

# Función para extraer el nombre de la especie de una línea
def extraer_especie(linea):
    match = re.search(r'([A-Za-z_]+)(?:_|$)', linea.split('_')[-1])
    if match:
        return match.group(1)
    return None

# Leer el primer archivo y almacenar las especies en un conjunto
with open(sys.argv[1], 'r') as f1:
    especies1 = {extraer_especie(line.strip()) for line in f1 if line.startswith('>')}

# Leer el segundo archivo y almacenar las especies en un conjunto
with open(sys.argv[2], 'r') as f2:
    especies2 = {extraer_especie(line.strip()) for line in f2 if line.startswith('>')}

print(f'Datos del archivo 1: {especies1}')
print(f'Datos del archivo 2: {especies2}')

# Encontrar las especies que aparecen en ambos archivos
especies_comunes = especies1.intersection(especies2)

# Escribir las especies comunes en un archivo de salida
with open('output.txt', 'w') as out:
    for especie in especies_comunes:
        out.write(f'>{especie}\n')
