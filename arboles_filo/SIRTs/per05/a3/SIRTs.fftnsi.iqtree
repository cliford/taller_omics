IQ-TREE 2.0.7 built Jan 21 2022

Input file name: SIRTs.fftnsi
Type of analysis: tree reconstruction
Random seed number: 702520

REFERENCES
----------

To cite IQ-TREE please use:

Bui Quang Minh, Heiko A. Schmidt, Olga Chernomor, Dominik Schrempf,
Michael D. Woodhams, Arndt von Haeseler, and Robert Lanfear (2020)
IQ-TREE 2: New models and efficient methods for phylogenetic inference
in the genomic era. Mol. Biol. Evol., in press.
https://doi.org/10.1093/molbev/msaa015

SEQUENCE ALIGNMENT
------------------

Input data: 150 sequences with 1669 amino-acid sites
Number of constant sites: 654 (= 39.1851% of all sites)
Number of invariant (constant or ambiguous constant) sites: 654 (= 39.1851% of all sites)
Number of parsimony informative sites: 808
Number of distinct site patterns: 1198

SUBSTITUTION PROCESS
--------------------

Model of substitution: JTT+F+I+G4

State frequencies: (empirical counts from alignment)

  pi(A) = 0.0691
  pi(R) = 0.0587
  pi(N) = 0.0353
  pi(D) = 0.0653
  pi(C) = 0.0266
  pi(Q) = 0.0362
  pi(E) = 0.0797
  pi(G) = 0.0692
  pi(H) = 0.0246
  pi(I) = 0.0548
  pi(L) = 0.0884
  pi(K) = 0.0477
  pi(M) = 0.0179
  pi(F) = 0.0414
  pi(P) = 0.0737
  pi(S) = 0.0783
  pi(T) = 0.0423
  pi(W) = 0.0091
  pi(Y) = 0.0242
  pi(V) = 0.0576

Model of rate heterogeneity: Invar+Gamma with 4 categories
Proportion of invariable sites: 0.0335
Gamma shape alpha: 0.7647

 Category  Relative_rate  Proportion
  0         0              0.0335
  1         0.0911         0.2416
  2         0.4061         0.2416
  3         0.9795         0.2416
  4         2.6620         0.2416
Relative rates are computed as MEAN of the portion of the Gamma distribution falling in the category.

MAXIMUM LIKELIHOOD TREE
-----------------------

Log-likelihood of the tree: -59289.9749 (s.e. 1701.5623)
Unconstrained log-likelihood (without tree): -10775.6353
Number of free parameters (#branches + #model parameters): 318
Akaike information criterion (AIC) score: 119215.9498
Corrected Akaike information criterion (AICc) score: 119366.2342
Bayesian information criterion (BIC) score: 120939.5034

Total tree length (sum of branch lengths): 68.6252
Sum of internal branch lengths: 27.7039 (40.3699% of tree length)

NOTE: Tree is UNROOTED although outgroup taxon 'SIRT1_ANAPP23028_U3I9B7_Anas_platyrhynchos_platyrhynchos' is drawn at root

+--SIRT1_ANAPP23028_U3I9B7_Anas_platyrhynchos_platyrhynchos
|
|              +--SIRT1_ANOCA17241_ENSACAG00000010558_Anolis_carolinensis
|           +--|
|           |  |  +--SIRT1_PODMU36783_A0A670IVI5_Podarcis_muralis
|           |  +--|
|           |     +--SIRT1_PSETE18241_A0A670YFV4_Pseudonaja_textilis
|        +--|
|        |  +--SIRT1_SPHPU13742_ENSSPUG00000001982.1_Sphenodon_punctatus
|     +--|
|     |  |                                      +--SIRT1_BALMU15616_ENSBMSG00010012923.1_Balaenoptera_musculus
|     |  |                                   +--|
|     |  |                                   |  +--SIRT1_BOVIN22421_F1MQB8_Bos_taurus
|     |  |                                +--|
|     |  |                                |  +--SIRT1_PIGXX07692_A0A4X1TRZ2_Sus_scrofa
|     |  |                             +--|
|     |  |                             |  |  +--SIRT1_Myotis_lucifugus_XM_006089727.3
|     |  |                             |  +--|
|     |  |                             |     +--SIRT1_PTEVA07651_ENSPVAG00000007129_Pteropus_vampyrus
|     |  |                          +--|
|     |  |                          |  +--SIRT1_Equus_caballus_XM_023643979.1
|     |  |                       +--|
|     |  |                       |  |  +--SIRT1_CANLF14607_E2RE73_Canis_lupus_familiaris
|     |  |                       |  +--|
|     |  |                       |     |  +--SIRT1_Felis_catus_NM_001290246.1
|     |  |                       |     +--|
|     |  |                       |        +--SIRT1_SURSU31617_A0A673TM53_Suricata_suricatta
|     |  |                    +--|
|     |  |                    |  +--SIRT1_MANJA11789_XP_036866792_Manis_javanica
|     |  |                 +--|
|     |  |                 |  |  +--SIRT1_Erinaceus_europaeus_XM_016191849.1
|     |  |                 |  +--|
|     |  |                 |     +--SIRT1_SORAR07806_ENSSARG00000005522_Sorex_araneus
|     |  |              +--|
|     |  |              |  |        +--SIRT1_CALJA05085_ENSCJAG00000019318_Callithrix_jacchus
|     |  |              |  |     +--|
|     |  |              |  |     |  |  +--SIRT1_HUMAN01330_SIR1_HUMAN_Homo_sapiens
|     |  |              |  |     |  +--|
|     |  |              |  |     |     +--SIRT1_MACNE10001_A0A2K6CDP5_Macaca_nemestrina
|     |  |              |  |  +--|
|     |  |              |  |  |  |        +--SIRT1_Cavia_porcellus_XM_023566684.1
|     |  |              |  |  |  |     +--|
|     |  |              |  |  |  |     |  |  +--SIRT1_MOUSE00326_SIR1_MOUSE_Mus_musculus
|     |  |              |  |  |  |     |  +--|
|     |  |              |  |  |  |     |     +--SIRT1_RATNO12227_A0A182DWI7_Rattus_norvegicus
|     |  |              |  |  |  |  +--|
|     |  |              |  |  |  |  |  +--SIRT1_Ictidomys_tridecemlineatus_XM_040273039.1
|     |  |              |  |  |  +--|
|     |  |              |  |  |     +--SIRT1_Dipodomys_ordii_XM_013017208.1
|     |  |              |  +--|
|     |  |              |     +--SIRT1_RABIT06761_G1U0D5_Oryctolagus_cuniculus
|     |  |           +--|
|     |  |           |  |  +--SIRT1_ECHTE04737_ENSETEG00000015512_Echinops_telfairi
|     |  |           |  +--|
|     |  |           |     +--SIRT1_Loxodonta_africana_XM_023546989.1
|     |  |        +--|
|     |  |        |  +--SIRT1_DASNO00242_ENSDNOG00000007191_Dasypus_novemcinctus
|     |  |     +--|
|     |  |     |  |        +--SIRT1_PHACI06199_ENSPCIG00000014645.1_Phascolarctos_cinereus
|     |  |     |  |     +--|
|     |  |     |  |     |  +--SIRT1_VOMUR15393_A0A4X2L8K8_Vombatus_ursinus
|     |  |     |  |  +--|
|     |  |     |  |  |  +--SIRT1_Sarcophilus_harrisii_XM_031956888.1
|     |  |     |  +--|
|     |  |     |     +--SIRT1_Ornithorhynchus_anatinus_XM_029059838.1
|     |  |  +--|
|     |  |  |  |                    +--SIRT1_CALMI02202_A0A4W3GSR4_Callorhinchus_milii
|     |  |  |  |                 +--|
|     |  |  |  |                 |  |        +--SIRT1_Carcharodon_carcharias_XM_041210202.1
|     |  |  |  |                 |  |     +--|
|     |  |  |  |                 |  |     |  +--SIRT1_Scyliorhinus_canicula_XM_038821818.1
|     |  |  |  |                 |  |  +--|
|     |  |  |  |                 |  |  |  |  +--SIRT1_Chiloscyllium_plagiosum_XM_043712270.1
|     |  |  |  |                 |  |  |  +--|
|     |  |  |  |                 |  |  |     +--SIRT1_Rhincodon_typus_XM_020511459.1
|     |  |  |  |                 |  +--|
|     |  |  |  |                 |     +--SIRT1_Amblyraja_radiata_XM_033034263.1
|     |  |  |  |              +--|
|     |  |  |  |              |  |     +--SIRT1_DANRE05946_E7F8W3_Danio_rerio
|     |  |  |  |              |  |  +--|
|     |  |  |  |              |  |  |  |     +--SIRT1_ORENI15860_ENSONIG00000010273_Oreochromis_niloticus
|     |  |  |  |              |  |  |  |  +--|
|     |  |  |  |              |  |  |  |  |  +--SIRT1_Takifugu_rubripes_XM_003963728.3
|     |  |  |  |              |  |  |  +--|
|     |  |  |  |              |  |  |     |  +--SIRT1_Oryzias_latipes_XM_004077504.4
|     |  |  |  |              |  |  |     +--|
|     |  |  |  |              |  |  |        +--SIRT1_POEFO14333_A0A087XX78_Poecilia_formosa
|     |  |  |  |              |  +--|
|     |  |  |  |              |     |  +--SIRT1_LEPOC13895_W5MZP3_Lepisosteus_oculatus
|     |  |  |  |              |     +--|
|     |  |  |  |              |        +--SIRT1_Erpetoichthys_calabaricus_XM_028795918.1
|     |  |  |  |           +--|
|     |  |  |  |           |  +--SIRT1_West_African_lungfish_Protopterus_annectens_XM_044056380.1
|     |  |  |  |        +--|
|     |  |  |  |        |  +--SIRT1_LATCH03255_H3AHF8_Latimeria_chalumnae
|     |  |  |  |     +--|
|     |  |  |  |     |  |     +---------SIRT1_Petromyzon_marinus_filaggrin-like_XM_032978785.1
|     |  |  |  |     |  +-----|
|     |  |  |  |     |        |                              +-----SIRT3_CALMI44714_A0A4W3HM86_Callorhinchus_milii
|     |  |  |  |     |        |                           +--|
|     |  |  |  |     |        |                           |  |        +--SIRT3-like_Scyliorhinus_canicula_XM_038808032.1
|     |  |  |  |     |        |                           |  |     +--|
|     |  |  |  |     |        |                           |  |     |  +--SIRT3_Carcharodon_carcharias_XM_041197000.1
|     |  |  |  |     |        |                           |  |  +--|
|     |  |  |  |     |        |                           |  |  |  |  +--SIRT3_Chiloscyllium_plagiosum_XM_043705713.1
|     |  |  |  |     |        |                           |  |  |  +--|
|     |  |  |  |     |        |                           |  |  |     +--SIRT3_Rhincodon_typus_XM_020528742.1
|     |  |  |  |     |        |                           |  +--|
|     |  |  |  |     |        |                           |     +---------SIRT3_Amblyraja_radiata_XM_033038599.1
|     |  |  |  |     |        |                        +--|
|     |  |  |  |     |        |                        |  |                       +--SIRT3_GASAC00595_G3PT13_Gasterosteus_aculeatus
|     |  |  |  |     |        |                        |  |                    +--|
|     |  |  |  |     |        |                        |  |                    |  +--SIRT3_Oreochromis_niloticus_XM_013275996.3
|     |  |  |  |     |        |                        |  |                 +--|
|     |  |  |  |     |        |                        |  |                 |  |  +--SIRT3_Poecilia_formosa_XM_007576827.2
|     |  |  |  |     |        |                        |  |                 |  +--|
|     |  |  |  |     |        |                        |  |                 |     +--SIRT3_Takifugu_rubripes_XM_011609813.2
|     |  |  |  |     |        |                        |  |              +--|
|     |  |  |  |     |        |                        |  |              |  +--SIRT3_Gadus_morhua_XM_030376087.1
|     |  |  |  |     |        |                        |  |           +--|
|     |  |  |  |     |        |                        |  |           |  +--SIRT3_DANRE39175_ENSDARG00000035819.6_Danio_rerio
|     |  |  |  |     |        |                        |  |        +--|
|     |  |  |  |     |        |                        |  |        |  +--SIRT3_LEPOC10482_W5M391_Lepisosteus_oculatus
|     |  |  |  |     |        |                        |  |     +--|
|     |  |  |  |     |        |                        |  |     |  +-----SIRT3_Erpetoichthys_calabaricus_XM_028796144.1
|     |  |  |  |     |        |                        |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |                                      +--SIRT3_HUMAN03534_SIR3_HUMAN_Homo_sapiens
|     |  |  |  |     |        |                        |  |  |  |                                   +--|
|     |  |  |  |     |        |                        |  |  |  |                                   |  +--SIRT3_CALJA04362_ENSCJAG00000005639_Callithrix_jacchus
|     |  |  |  |     |        |                        |  |  |  |                                +--|
|     |  |  |  |     |        |                        |  |  |  |                                |  +--SIRT3_MACFA08111_ENSMFAG00000041754.1_Macaca_fascicularis
|     |  |  |  |     |        |                        |  |  |  |                             +--|
|     |  |  |  |     |        |                        |  |  |  |                             |  +--SIRT3_RABIT18645_C6ZII8_Oryctolagus_cuniculus
|     |  |  |  |     |        |                        |  |  |  |                          +--|
|     |  |  |  |     |        |                        |  |  |  |                          |  +--SIRT3_Tupaia_belangeri_XM_027766673.1
|     |  |  |  |     |        |                        |  |  |  |                       +--|
|     |  |  |  |     |        |                        |  |  |  |                       |  |           +--SIRT3_BOVIN02950_G5E521_Bos_taurus
|     |  |  |  |     |        |                        |  |  |  |                       |  |        +--|
|     |  |  |  |     |        |                        |  |  |  |                       |  |        |  +--SIRT3_BALMU09897_ENSBMSG00010009744.1_Balaenoptera_musculus
|     |  |  |  |     |        |                        |  |  |  |                       |  |     +--|
|     |  |  |  |     |        |                        |  |  |  |                       |  |     |  +--SIRT3_PIGXX16458_A0A480QI11_Sus_scrofa
|     |  |  |  |     |        |                        |  |  |  |                       |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |                       |  |  |  |  +--SIRT3_CANLF05591_F6Y2M8_Canis_lupus_familiaris
|     |  |  |  |     |        |                        |  |  |  |                       |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |                       |  |  |     |  +--SIRT3_FELCA12499_ENSFCAG00000002668_Felis_catus
|     |  |  |  |     |        |                        |  |  |  |                       |  |  |     +--|
|     |  |  |  |     |        |                        |  |  |  |                       |  |  |        +--SIRT3_SURSU34923_A0A673TCM6_Suricata_suricatta
|     |  |  |  |     |        |                        |  |  |  |                       |  +--|
|     |  |  |  |     |        |                        |  |  |  |                       |     |  +--SIRT3_PTEVA04051_ENSPVAG00000017703_Pteropus_vampyrus
|     |  |  |  |     |        |                        |  |  |  |                       |     +--|
|     |  |  |  |     |        |                        |  |  |  |                       |        |  +--SIRT3_Equus_caballus_XM_023654877.1
|     |  |  |  |     |        |                        |  |  |  |                       |        +--|
|     |  |  |  |     |        |                        |  |  |  |                       |           +--SIRT3_MANJA50844_XP_036858345_Manis_javanica
|     |  |  |  |     |        |                        |  |  |  |                    +--|
|     |  |  |  |     |        |                        |  |  |  |                    |  |  +--SIRT3_CAVAP18143_ENSCAPG00000014228.1_Cavia_aperea
|     |  |  |  |     |        |                        |  |  |  |                    |  +--|
|     |  |  |  |     |        |                        |  |  |  |                    |     +--SIRT3_Ictidomys_tridecemlineatus_XM_005341607.4
|     |  |  |  |     |        |                        |  |  |  |                 +--|
|     |  |  |  |     |        |                        |  |  |  |                 |  |  +--SIRT3_MOUSE56805_SIR3_MOUSE_Mus_musculus
|     |  |  |  |     |        |                        |  |  |  |                 |  +--|
|     |  |  |  |     |        |                        |  |  |  |                 |     +--SIRT3_RATNO02156_C6ZII9_Rattus_norvegicus
|     |  |  |  |     |        |                        |  |  |  |              +--|
|     |  |  |  |     |        |                        |  |  |  |              |  |  +--SIRT3_LOXAF00900_G3U401_Loxodonta_africana
|     |  |  |  |     |        |                        |  |  |  |              |  +--|
|     |  |  |  |     |        |                        |  |  |  |              |     +--SIRT3_Dasypus_novemcinctus_XM_023583621.1
|     |  |  |  |     |        |                        |  |  |  |           +--|
|     |  |  |  |     |        |                        |  |  |  |           |  |  +--SIRT3_Sarcophilus_harrisii_XM_031942757.1
|     |  |  |  |     |        |                        |  |  |  |           |  +--|
|     |  |  |  |     |        |                        |  |  |  |           |     |  +--SIRT3_PHACI31658_A0A6P5JNH1_Phascolarctos_cinereus
|     |  |  |  |     |        |                        |  |  |  |           |     +--|
|     |  |  |  |     |        |                        |  |  |  |           |        +--SIRT3_VOMUR29351_A0A4X2K1X1_Vombatus_ursinus
|     |  |  |  |     |        |                        |  |  |  |        +--|
|     |  |  |  |     |        |                        |  |  |  |        |  +---SIRT3_Ornithorhynchus_anatinus_XM_029060503.2
|     |  |  |  |     |        |                        |  |  |  |     +--|
|     |  |  |  |     |        |                        |  |  |  |     |  |  +--SIRT3_Microcaecilia_unicolor_XM_030201197.1
|     |  |  |  |     |        |                        |  |  |  |     |  +--|
|     |  |  |  |     |        |                        |  |  |  |     |     +--SIRT3_Rhinatrema_bivittatum_XM_029582817.1
|     |  |  |  |     |        |                        |  |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |  |        +--SIRT3_ANAPP17745_U3J7C6_Anas_platyrhynchos_platyrhynchos
|     |  |  |  |     |        |                        |  |  |  |  |  |     +--|
|     |  |  |  |     |        |                        |  |  |  |  |  |     |  |  +--SIRT3_CHICK21600_A0A1D5NYI2_Gallus_gallus
|     |  |  |  |     |        |                        |  |  |  |  |  |     |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |  |     |     +--SIRT3_PHACC16976_A0A669QJP1_Phasianus_colchicus
|     |  |  |  |     |        |                        |  |  |  |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |  |  |  |  +--SIRT3_FICAL12686_U3KAA2_Ficedula_albicollis
|     |  |  |  |     |        |                        |  |  |  |  |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |  |  |     +--SIRT3_TAEGU10236_ENSTGUG00000006886_Taeniopygia_guttata
|     |  |  |  |     |        |                        |  |  |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |     |     +--SIRT3_CHRPI28225_ENSCPBG00000022149.1_Chrysemys_picta_bellii
|     |  |  |  |     |        |                        |  |  |  |  |     |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |     |  |  |     +--SIRT3_PODMU07057_A0A670HQC6_Podarcis_muralis
|     |  |  |  |     |        |                        |  |  |  |  |     |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |     |  |  |  |  +--SIRT3_PSETE12110_A0A670Y173_Pseudonaja_textilis
|     |  |  |  |     |        |                        |  |  |  |  |     |  |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |     |  |     +--SIRT3_ANOCA00588_G1KU17_Anolis_carolinensis
|     |  |  |  |     |        |                        |  |  |  |  |     +--|
|     |  |  |  |     |        |                        |  |  |  |  |        |     +--SIRT3_Alligator_mississippiensis_XM_006274883.3
|     |  |  |  |     |        |                        |  |  |  |  |        |  +--|
|     |  |  |  |     |        |                        |  |  |  |  |        |  |  +--SIRT3_Alligator_sinensis_XM_006033338.3
|     |  |  |  |     |        |                        |  |  |  |  |        +--|
|     |  |  |  |     |        |                        |  |  |  |  |           |  +--SIRT3_Gavialis_gangeticus_XM_019519864.1
|     |  |  |  |     |        |                        |  |  |  |  |           +--|
|     |  |  |  |     |        |                        |  |  |  |  |              +--SIRT3_Crocodylus_porosus_XM_019547230.1
|     |  |  |  |     |        |                        |  |  |  +--|
|     |  |  |  |     |        |                        |  |  |     +------SIRT3_West_African_lungfish_Protopterus_annectens_XM_044082822.1
|     |  |  |  |     |        |                        |  +--|
|     |  |  |  |     |        |                        |     +-----SIRT3_LATCH03716_H3AAK4_Latimeria_chalumnae
|     |  |  |  |     |        +------------------------|
|     |  |  |  |     |                                 |     +----------SIRT3_EPTBU18760_ENSEBUG00000003693.1_Eptatretus_burgeri
|     |  |  |  |     |                                 |  +--|
|     |  |  |  |     |                                 |  |  |           +--SIRT3_Xenopus_tropicalis_XM_012962148.3
|     |  |  |  |     |                                 |  |  |        +--|
|     |  |  |  |     |                                 |  |  |        |  +--SIRT3_XENLA19366_A0A1L8GDN0_Xenopus_laevis
|     |  |  |  |     |                                 |  |  +--------|
|     |  |  |  |     |                                 |  |           |     +--SIRT3_Nanorana_parkeri_XM_018567219.1
|     |  |  |  |     |                                 |  |           |  +--|
|     |  |  |  |     |                                 |  |           |  |  +--SIRT3_Rana_temporaria_XM_040328438.1
|     |  |  |  |     |                                 |  |           +--|
|     |  |  |  |     |                                 |  |              +--SIRT3_Bufo_bufo_XM_040409401.1
|     |  |  |  |     |                                 +--|
|     |  |  |  |     |                                    |  +--------SIRT3_Petromyzon_marinus_XM_032949194.1
|     |  |  |  |     |                                    +--|
|     |  |  |  |     |                                       |                        +--SIRT3-like_Xenopus_laevis_NM_001096098.1
|     |  |  |  |     |                                       |                +-------|
|     |  |  |  |     |                                       |                |       +--SIRT3-like_Xenopus_tropicalis_XM_031897650.1
|     |  |  |  |     |                                       |             +--|
|     |  |  |  |     |                                       |             |  |     +--SIRT3-like_Nanorana_parkeri_XM_018557664.1
|     |  |  |  |     |                                       |             |  |  +--|
|     |  |  |  |     |                                       |             |  |  |  +--SIRT3-like_Rana_temporaria_XM_040345208.1
|     |  |  |  |     |                                       |             |  +--|
|     |  |  |  |     |                                       |             |     +--SIRT3-like_Bufo_bufo_XM_040439569.1
|     |  |  |  |     |                                       |          +--|
|     |  |  |  |     |                                       |          |  +---------SIRT3_like_West_African_lungfish_Protopterus_annectens_XM_044088148.1
|     |  |  |  |     |                                       |       +--|
|     |  |  |  |     |                                       |       |  |                         +--SIRT3-like_Poecilia_formosa_ENSPFOG00000016528
|     |  |  |  |     |                                       |       |  |                      +--|
|     |  |  |  |     |                                       |       |  |                      |  +--SIRT3-like_Xiphophorus_maculatus_ENSXMAG00000001675
|     |  |  |  |     |                                       |       |  |                   +--|
|     |  |  |  |     |                                       |       |  |                   |  +--SIRT3-like_Oryzias_latipes_ENSORLG00000030051
|     |  |  |  |     |                                       |       |  |                +--|
|     |  |  |  |     |                                       |       |  |                |  |  +--SIRT3-like_Oreochromis_aureus_ENSOABG00000011452
|     |  |  |  |     |                                       |       |  |                |  +--|
|     |  |  |  |     |                                       |       |  |                |     +--SIRT3-like_Gasterosteus_aculeatus_ENSGACG00000011838
|     |  |  |  |     |                                       |       |  |             +--|
|     |  |  |  |     |                                       |       |  |             |  +--SIRT3-like_Fugu_ENSTRUG00000009380
|     |  |  |  |     |                                       |       |  |          +--|
|     |  |  |  |     |                                       |       |  |          |  +----SIRT3-like_Gadus_morua_ENSGMOG00000004353
|     |  |  |  |     |                                       |       |  |     +----|
|     |  |  |  |     |                                       |       |  |     |    |  +--SIRT3-like_Astyanaxmexicanus_ENSAMXG00000020124
|     |  |  |  |     |                                       |       |  |     |    +--|
|     |  |  |  |     |                                       |       |  |     |       +--SIRT3-like_Danio_rerio_ENSDARG00000062893
|     |  |  |  |     |                                       |       |  |  +--|
|     |  |  |  |     |                                       |       |  |  |  +---SIRT3-like_Lepisosteusoculatus_XM_015351860.1
|     |  |  |  |     |                                       |       |  +--|
|     |  |  |  |     |                                       |       |     +-------SIRT3-like_Erpetoichthys_calabaricus_XM_028816076.1
|     |  |  |  |     |                                       |    +--|
|     |  |  |  |     |                                       |    |  +-----SIRT3-like_Latimeria_chalumnae_ENSLACG00000017633
|     |  |  |  |     |                                       +----|
|     |  |  |  |     |                                            |  +----SIRT3-like_Elephant_fish_NW_024704746.1_c30371848-30354578
|     |  |  |  |     |                                            +--|
|     |  |  |  |     |                                               +---------SIRT3-like_Carcharodon_carcharias_XM_041215676.1_LOC557125
|     |  |  |  |  +--|
|     |  |  |  |  |  |        +--SIRT1_XENLA29634_A0A1L8FJP8_Xenopus_laevis
|     |  |  |  |  |  |     +--|
|     |  |  |  |  |  |     |  +--SIRT1_XENLA31850_A0A1L8FEV8_Xenopus_laevis
|     |  |  |  |  |  |  +--|
|     |  |  |  |  |  |  |  +--SIRT1_XENTR08910_ENSXETG00000023588_Xenopus_tropicalis
|     |  |  |  |  |  +--|
|     |  |  |  |  |     |     +--SIRT1_Nanorana_parkeri_XM_018573518.1
|     |  |  |  |  |     |  +--|
|     |  |  |  |  |     |  |  +--SIRT1_Rana_temporaria_XM_040362158.1
|     |  |  |  |  |     +--|
|     |  |  |  |  |        +--SIRT1_Bufo_bufo_XM_040438305.1
|     |  |  |  +--|
|     |  |  |     |     +--SIRT1_Microcaecilia_unicolor_XM_030203130.1
|     |  |  |     |  +--|
|     |  |  |     |  |  +--SIRT1_Geotrypetes_seraphini_XM_033941657.1
|     |  |  |     +--|
|     |  |  |        +--SIRT1_Rhinatrema_bivittatum_XM_029609704.1
|     |  +--|
|     |     |     +--SIRT1_CHEAB04289_ENSCABG00000022942.1_Chelonoidis_abingdonii
|     |     |  +--|
|     |     |  |  +--SIRT1_CHRPI04679_ENSCPBG00000019531.1_Chrysemys_picta_bellii
|     |     +--|
|     |        +--SIRT1_Pelodiscus_sinensis_XM_006125276.3
|  +--|
|  |  |     +--SIRT1_Alligator_mississippiensis_XM_019484400.1
|  |  |  +--|
|  |  |  |  +**SIRT1_Alligator_sinensis_XM_006024809.3
|  |  +--|
|  |     |  +--SIRT1_Gavialis_gangeticus_XM_019507879.1
|  |     +--|
|  |        +--SIRT1_Crocodylus_porosus_XM_019538640.1
+--|
|  +--SIRT1_Gallus_gallus_NM_001004767.2
|
|  +--SIRT1_Ficedula_albicollis_XM_005047913.1
+--|
   +--SIRT1_Taeniopygia_guttata_XM_041717094.1

Tree in newick format:

(SIRT1_ANAPP23028_U3I9B7_Anas_platyrhynchos_platyrhynchos:0.0363351685,(((((SIRT1_ANOCA17241_ENSACAG00000010558_Anolis_carolinensis:0.1316039960,(SIRT1_PODMU36783_A0A670IVI5_Podarcis_muralis:0.0724528468,SIRT1_PSETE18241_A0A670YFV4_Pseudonaja_textilis:0.1665052929):0.0132380005):0.0870180567,SIRT1_SPHPU13742_ENSSPUG00000001982.1_Sphenodon_punctatus:0.1206319583):0.0544749264,(((((((((((((SIRT1_BALMU15616_ENSBMSG00010012923.1_Balaenoptera_musculus:0.0190731426,SIRT1_BOVIN22421_F1MQB8_Bos_taurus:0.0282239234):0.0025336147,SIRT1_PIGXX07692_A0A4X1TRZ2_Sus_scrofa:0.0306770130):0.0105023638,(SIRT1_Myotis_lucifugus_XM_006089727.3:0.0612716104,SIRT1_PTEVA07651_ENSPVAG00000007129_Pteropus_vampyrus:0.0412217234):0.0026888624):0.0015144855,SIRT1_Equus_caballus_XM_023643979.1:0.0341924710):0.0013073570,(SIRT1_CANLF14607_E2RE73_Canis_lupus_familiaris:0.0200914942,(SIRT1_Felis_catus_NM_001290246.1:0.0111219640,SIRT1_SURSU31617_A0A673TM53_Suricata_suricatta:0.0139063000):0.0047034501):0.0165687630):0.0021565608,SIRT1_MANJA11789_XP_036866792_Manis_javanica:0.0621933173):0.0019405818,(SIRT1_Erinaceus_europaeus_XM_016191849.1:0.0729884805,SIRT1_SORAR07806_ENSSARG00000005522_Sorex_araneus:0.0664177617):0.0310133874):0.0079539942,(((SIRT1_CALJA05085_ENSCJAG00000019318_Callithrix_jacchus:0.0252375663,(SIRT1_HUMAN01330_SIR1_HUMAN_Homo_sapiens:0.0121256665,SIRT1_MACNE10001_A0A2K6CDP5_Macaca_nemestrina:0.0106773714):0.0083675376):0.0211794853,(((SIRT1_Cavia_porcellus_XM_023566684.1:0.0483682927,(SIRT1_MOUSE00326_SIR1_MOUSE_Mus_musculus:0.0434866559,SIRT1_RATNO12227_A0A182DWI7_Rattus_norvegicus:0.1617318036):0.0437181091):0.0047377708,SIRT1_Ictidomys_tridecemlineatus_XM_040273039.1:0.0737017736):0.0076522282,SIRT1_Dipodomys_ordii_XM_013017208.1:0.0742404933):0.0105353187):0.0071360831,SIRT1_RABIT06761_G1U0D5_Oryctolagus_cuniculus:0.0549721097):0.0052968025):0.0050807789,(SIRT1_ECHTE04737_ENSETEG00000015512_Echinops_telfairi:0.0613028087,SIRT1_Loxodonta_africana_XM_023546989.1:0.0552821458):0.0034011283):0.0038047220,SIRT1_DASNO00242_ENSDNOG00000007191_Dasypus_novemcinctus:0.0657430419):0.0852624587,(((SIRT1_PHACI06199_ENSPCIG00000014645.1_Phascolarctos_cinereus:0.0089605374,SIRT1_VOMUR15393_A0A4X2L8K8_Vombatus_ursinus:0.0236783835):0.0204072411,SIRT1_Sarcophilus_harrisii_XM_031956888.1:0.0491823338):0.1165817530,SIRT1_Ornithorhynchus_anatinus_XM_029059838.1:0.1908247691):0.0378318340):0.0931243100,(((((((SIRT1_CALMI02202_A0A4W3GSR4_Callorhinchus_milii:0.2009951079,(((SIRT1_Carcharodon_carcharias_XM_041210202.1:0.0530272740,SIRT1_Scyliorhinus_canicula_XM_038821818.1:0.0590453813):0.0256773730,(SIRT1_Chiloscyllium_plagiosum_XM_043712270.1:0.0512838925,SIRT1_Rhincodon_typus_XM_020511459.1:0.0449051310):0.0894590199):0.0316307592,SIRT1_Amblyraja_radiata_XM_033034263.1:0.1809617477):0.0693611923):0.4431784427,((SIRT1_DANRE05946_E7F8W3_Danio_rerio:0.5135494812,((SIRT1_ORENI15860_ENSONIG00000010273_Oreochromis_niloticus:0.0970599877,SIRT1_Takifugu_rubripes_XM_003963728.3:0.1945767290):0.0289575575,(SIRT1_Oryzias_latipes_XM_004077504.4:0.2168551573,SIRT1_POEFO14333_A0A087XX78_Poecilia_formosa:0.1920294783):0.0318274253):0.2700700718):0.2378601533,(SIRT1_LEPOC13895_W5MZP3_Lepisosteus_oculatus:0.2169067473,SIRT1_Erpetoichthys_calabaricus_XM_028795918.1:0.3397153438):0.0736584997):0.1930783385):0.0292950879,SIRT1_West_African_lungfish_Protopterus_annectens_XM_044056380.1:0.3922254404):0.0320567919,SIRT1_LATCH03255_H3AHF8_Latimeria_chalumnae:0.5675618414):0.0556361582,(SIRT1_Petromyzon_marinus_filaggrin-like_XM_032978785.1:1.5650747532,(((SIRT3_CALMI44714_A0A4W3HM86_Callorhinchus_milii:0.8954488049,(((SIRT3-like_Scyliorhinus_canicula_XM_038808032.1:0.1764191432,SIRT3_Carcharodon_carcharias_XM_041197000.1:0.1131124829):0.1075475763,(SIRT3_Chiloscyllium_plagiosum_XM_043705713.1:0.1125738257,SIRT3_Rhincodon_typus_XM_020528742.1:0.0662549496):0.1019873828):0.5398800349,SIRT3_Amblyraja_radiata_XM_033038599.1:1.5005932256):0.2607993367):0.5357045305,((((((((SIRT3_GASAC00595_G3PT13_Gasterosteus_aculeatus:0.5727766795,SIRT3_Oreochromis_niloticus_XM_013275996.3:0.2411584753):0.0514052810,(SIRT3_Poecilia_formosa_XM_007576827.2:0.4389108285,SIRT3_Takifugu_rubripes_XM_011609813.2:0.3379705090):0.1187892046):0.0862950871,SIRT3_Gadus_morhua_XM_030376087.1:0.5918685462):0.2649847930,SIRT3_DANRE39175_ENSDARG00000035819.6_Danio_rerio:0.4560818416):0.3871115550,SIRT3_LEPOC10482_W5M391_Lepisosteus_oculatus:0.5651575923):0.1695832287,SIRT3_Erpetoichthys_calabaricus_XM_028796144.1:0.9436865053):0.3003038059,(((((((((((((SIRT3_HUMAN03534_SIR3_HUMAN_Homo_sapiens:0.0455043638,SIRT3_CALJA04362_ENSCJAG00000005639_Callithrix_jacchus:0.1516708275):0.0080340821,SIRT3_MACFA08111_ENSMFAG00000041754.1_Macaca_fascicularis:0.0693526410):0.2024316041,SIRT3_RABIT18645_C6ZII8_Oryctolagus_cuniculus:0.3840241228):0.0049073965,SIRT3_Tupaia_belangeri_XM_027766673.1:0.3100996094):0.0554851337,((((SIRT3_BOVIN02950_G5E521_Bos_taurus:0.3550952420,SIRT3_BALMU09897_ENSBMSG00010009744.1_Balaenoptera_musculus:0.1757533466):0.0462316260,SIRT3_PIGXX16458_A0A480QI11_Sus_scrofa:0.1924511968):0.2430519818,(SIRT3_CANLF05591_F6Y2M8_Canis_lupus_familiaris:0.2418664521,(SIRT3_FELCA12499_ENSFCAG00000002668_Felis_catus:0.2210132973,SIRT3_SURSU34923_A0A673TCM6_Suricata_suricatta:0.0734607209):0.0844428888):0.2556112480):0.0341130519,(SIRT3_PTEVA04051_ENSPVAG00000017703_Pteropus_vampyrus:0.4325024059,(SIRT3_Equus_caballus_XM_023654877.1:0.2066471504,SIRT3_MANJA50844_XP_036858345_Manis_javanica:0.4436947065):0.0136801478):0.0587163950):0.0397737537):0.0421454608,(SIRT3_CAVAP18143_ENSCAPG00000014228.1_Cavia_aperea:0.3526350121,SIRT3_Ictidomys_tridecemlineatus_XM_005341607.4:0.1367418412):0.1196043124):0.0442551470,(SIRT3_MOUSE56805_SIR3_MOUSE_Mus_musculus:0.1143579820,SIRT3_RATNO02156_C6ZII9_Rattus_norvegicus:0.1065038636):0.3263244133):0.0589266181,(SIRT3_LOXAF00900_G3U401_Loxodonta_africana:0.4749000762,SIRT3_Dasypus_novemcinctus_XM_023583621.1:0.4832750563):0.0989360801):0.2317598626,(SIRT3_Sarcophilus_harrisii_XM_031942757.1:0.3609254335,(SIRT3_PHACI31658_A0A6P5JNH1_Phascolarctos_cinereus:0.0362057996,SIRT3_VOMUR29351_A0A4X2K1X1_Vombatus_ursinus:0.0913995135):0.3301751379):0.4524498566):0.1422647429,SIRT3_Ornithorhynchus_anatinus_XM_029060503.2:0.6885279707):0.1408062475,(SIRT3_Microcaecilia_unicolor_XM_030201197.1:0.3369968052,SIRT3_Rhinatrema_bivittatum_XM_029582817.1:0.2338766932):0.4079220721):0.0998644259,(((SIRT3_ANAPP17745_U3J7C6_Anas_platyrhynchos_platyrhynchos:0.1912658443,(SIRT3_CHICK21600_A0A1D5NYI2_Gallus_gallus:0.0152528244,SIRT3_PHACC16976_A0A669QJP1_Phasianus_colchicus:0.0675739824):0.1209367606):0.0574947515,(SIRT3_FICAL12686_U3KAA2_Ficedula_albicollis:0.1320566273,SIRT3_TAEGU10236_ENSTGUG00000006886_Taeniopygia_guttata:0.0762734628):0.1734761332):0.3010449135,((SIRT3_CHRPI28225_ENSCPBG00000022149.1_Chrysemys_picta_bellii:0.2458374544,((SIRT3_PODMU07057_A0A670HQC6_Podarcis_muralis:0.3169752473,SIRT3_PSETE12110_A0A670Y173_Pseudonaja_textilis:0.4417166823):0.0488264604,SIRT3_ANOCA00588_G1KU17_Anolis_carolinensis:0.3570333587):0.2963993742):0.0574918561,((SIRT3_Alligator_mississippiensis_XM_006274883.3:0.0282250919,SIRT3_Alligator_sinensis_XM_006033338.3:0.0258986927):0.0143515542,(SIRT3_Gavialis_gangeticus_XM_019519864.1:0.0268034519,SIRT3_Crocodylus_porosus_XM_019547230.1:0.0262165441):0.0338945818):0.2750482299):0.1323305438):0.1385948358):0.3020933749,SIRT3_West_African_lungfish_Protopterus_annectens_XM_044082822.1:1.1844685581):0.2202023686):0.1284453529,SIRT3_LATCH03716_H3AAK4_Latimeria_chalumnae:0.9133812574):0.1424910069):0.3282388286,((SIRT3_EPTBU18760_ENSEBUG00000003693.1_Eptatretus_burgeri:1.7071171661,((SIRT3_Xenopus_tropicalis_XM_012962148.3:0.2107727193,SIRT3_XENLA19366_A0A1L8GDN0_Xenopus_laevis:0.1103559084):0.5799011968,((SIRT3_Nanorana_parkeri_XM_018567219.1:0.1592442795,SIRT3_Rana_temporaria_XM_040328438.1:0.2813993917):0.3279206885,SIRT3_Bufo_bufo_XM_040409401.1:0.4079481092):0.3313560301):1.4771694360):0.4297734252,(SIRT3_Petromyzon_marinus_XM_032949194.1:1.4659026603,((((((SIRT3-like_Xenopus_laevis_NM_001096098.1:0.2304420423,SIRT3-like_Xenopus_tropicalis_XM_031897650.1:0.1585704046):1.2405950440,((SIRT3-like_Nanorana_parkeri_XM_018557664.1:0.3052654816,SIRT3-like_Rana_temporaria_XM_040345208.1:0.3065105389):0.3660231047,SIRT3-like_Bufo_bufo_XM_040439569.1:0.4250624542):0.3223334425):0.2962647761,SIRT3_like_West_African_lungfish_Protopterus_annectens_XM_044088148.1:1.6011807610):0.1703578761,((((((((SIRT3-like_Poecilia_formosa_ENSPFOG00000016528:0.0356085306,SIRT3-like_Xiphophorus_maculatus_ENSXMAG00000001675:0.0237549401):0.3136356825,SIRT3-like_Oryzias_latipes_ENSORLG00000030051:0.5032410285):0.0763740994,(SIRT3-like_Oreochromis_aureus_ENSOABG00000011452:0.1218124419,SIRT3-like_Gasterosteus_aculeatus_ENSGACG00000011838:0.4284535088):0.0393902473):0.0468897481,SIRT3-like_Fugu_ENSTRUG00000009380:0.3258198256):0.1263288872,SIRT3-like_Gadus_morua_ENSGMOG00000004353:0.8172986697):0.0879615828,(SIRT3-like_Astyanaxmexicanus_ENSAMXG00000020124:0.2357460754,SIRT3-like_Danio_rerio_ENSDARG00000062893:0.3440872488):0.3183367090):0.8160634433,SIRT3-like_Lepisosteusoculatus_XM_015351860.1:0.5942697203):0.3062585571,SIRT3-like_Erpetoichthys_calabaricus_XM_028816076.1:1.3169508269):0.2383094423):0.1327575543,SIRT3-like_Latimeria_chalumnae_ENSLACG00000017633:0.9491504926):0.0615684871,(SIRT3-like_Elephant_fish_NW_024704746.1_c30371848-30354578:0.8872059540,SIRT3-like_Carcharodon_carcharias_XM_041215676.1_LOC557125:1.5477983632):0.3189320680):0.8088394159):0.3140337781):0.1558762289):3.8088840763):0.9399242704):0.0759904026,(((SIRT1_XENLA29634_A0A1L8FJP8_Xenopus_laevis:0.0278966622,SIRT1_XENLA31850_A0A1L8FEV8_Xenopus_laevis:0.0591563132):0.0127678302,SIRT1_XENTR08910_ENSXETG00000023588_Xenopus_tropicalis:0.0319936727):0.2435974310,((SIRT1_Nanorana_parkeri_XM_018573518.1:0.0479915923,SIRT1_Rana_temporaria_XM_040362158.1:0.0621492919):0.1430597052,SIRT1_Bufo_bufo_XM_040438305.1:0.1420064733):0.1471968339):0.3072529540):0.0419160929,((SIRT1_Microcaecilia_unicolor_XM_030203130.1:0.0440059505,SIRT1_Geotrypetes_seraphini_XM_033941657.1:0.0524916151):0.0436096222,SIRT1_Rhinatrema_bivittatum_XM_029609704.1:0.0396802906):0.1631849252):0.0739543207):0.1049612207,((SIRT1_CHEAB04289_ENSCABG00000022942.1_Chelonoidis_abingdonii:0.0294336576,SIRT1_CHRPI04679_ENSCPBG00000019531.1_Chrysemys_picta_bellii:0.0191577477):0.0263397065,SIRT1_Pelodiscus_sinensis_XM_006125276.3:0.0855337886):0.0329929390):0.0201037563):0.0125338544,((SIRT1_Alligator_mississippiensis_XM_019484400.1:0.0225288231,SIRT1_Alligator_sinensis_XM_006024809.3:0.0000029303):0.0026682918,(SIRT1_Gavialis_gangeticus_XM_019507879.1:0.0040318519,SIRT1_Crocodylus_porosus_XM_019538640.1:0.0135163907):0.0050189396):0.0619116108):0.0837020487,SIRT1_Gallus_gallus_NM_001004767.2:0.0549467686):0.0114723687,(SIRT1_Ficedula_albicollis_XM_005047913.1:0.0477319452,SIRT1_Taeniopygia_guttata_XM_041717094.1:0.0213605431):0.0764979482);

TIME STAMP
----------

Date and time: Mon Sep 11 13:25:26 2023
Total CPU time used: 13234.1 seconds (3h:40m:34s)
Total wall-clock time used: 3344.63 seconds (0h:55m:44s)

