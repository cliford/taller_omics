IQ-TREE 2.0.7 built Jan 21 2022

Input file name: SIRTs.fftnsi
Type of analysis: tree reconstruction
Random seed number: 485624

REFERENCES
----------

To cite IQ-TREE please use:

Bui Quang Minh, Heiko A. Schmidt, Olga Chernomor, Dominik Schrempf,
Michael D. Woodhams, Arndt von Haeseler, and Robert Lanfear (2020)
IQ-TREE 2: New models and efficient methods for phylogenetic inference
in the genomic era. Mol. Biol. Evol., in press.
https://doi.org/10.1093/molbev/msaa015

SEQUENCE ALIGNMENT
------------------

Input data: 150 sequences with 1669 amino-acid sites
Number of constant sites: 654 (= 39.1851% of all sites)
Number of invariant (constant or ambiguous constant) sites: 654 (= 39.1851% of all sites)
Number of parsimony informative sites: 808
Number of distinct site patterns: 1198

SUBSTITUTION PROCESS
--------------------

Model of substitution: JTT+F+I+G4

State frequencies: (empirical counts from alignment)

  pi(A) = 0.0691
  pi(R) = 0.0587
  pi(N) = 0.0353
  pi(D) = 0.0653
  pi(C) = 0.0266
  pi(Q) = 0.0362
  pi(E) = 0.0797
  pi(G) = 0.0692
  pi(H) = 0.0246
  pi(I) = 0.0548
  pi(L) = 0.0884
  pi(K) = 0.0477
  pi(M) = 0.0179
  pi(F) = 0.0414
  pi(P) = 0.0737
  pi(S) = 0.0783
  pi(T) = 0.0423
  pi(W) = 0.0091
  pi(Y) = 0.0242
  pi(V) = 0.0576

Model of rate heterogeneity: Invar+Gamma with 4 categories
Proportion of invariable sites: 0.0337
Gamma shape alpha: 0.7656

 Category  Relative_rate  Proportion
  0         0              0.0337
  1         0.0913         0.2416
  2         0.4066         0.2416
  3         0.9800         0.2416
  4         2.6618         0.2416
Relative rates are computed as MEAN of the portion of the Gamma distribution falling in the category.

MAXIMUM LIKELIHOOD TREE
-----------------------

Log-likelihood of the tree: -59283.7298 (s.e. 1701.5067)
Unconstrained log-likelihood (without tree): -10775.6353
Number of free parameters (#branches + #model parameters): 318
Akaike information criterion (AIC) score: 119203.4595
Corrected Akaike information criterion (AICc) score: 119353.7439
Bayesian information criterion (BIC) score: 120927.0131

Total tree length (sum of branch lengths): 69.7622
Sum of internal branch lengths: 28.3787 (40.6791% of tree length)

NOTE: Tree is UNROOTED although outgroup taxon 'SIRT1_ANAPP23028_U3I9B7_Anas_platyrhynchos_platyrhynchos' is drawn at root

+--SIRT1_ANAPP23028_U3I9B7_Anas_platyrhynchos_platyrhynchos
|
|              +--SIRT1_ANOCA17241_ENSACAG00000010558_Anolis_carolinensis
|           +--|
|           |  |  +--SIRT1_PODMU36783_A0A670IVI5_Podarcis_muralis
|           |  +--|
|           |     +--SIRT1_PSETE18241_A0A670YFV4_Pseudonaja_textilis
|        +--|
|        |  +--SIRT1_SPHPU13742_ENSSPUG00000001982.1_Sphenodon_punctatus
|     +--|
|     |  |                                      +--SIRT1_BALMU15616_ENSBMSG00010012923.1_Balaenoptera_musculus
|     |  |                                   +--|
|     |  |                                   |  +--SIRT1_BOVIN22421_F1MQB8_Bos_taurus
|     |  |                                +--|
|     |  |                                |  +--SIRT1_PIGXX07692_A0A4X1TRZ2_Sus_scrofa
|     |  |                             +--|
|     |  |                             |  |  +--SIRT1_Myotis_lucifugus_XM_006089727.3
|     |  |                             |  +--|
|     |  |                             |     +--SIRT1_PTEVA07651_ENSPVAG00000007129_Pteropus_vampyrus
|     |  |                          +--|
|     |  |                          |  +--SIRT1_Equus_caballus_XM_023643979.1
|     |  |                       +--|
|     |  |                       |  |  +--SIRT1_CANLF14607_E2RE73_Canis_lupus_familiaris
|     |  |                       |  +--|
|     |  |                       |     |  +--SIRT1_Felis_catus_NM_001290246.1
|     |  |                       |     +--|
|     |  |                       |        +--SIRT1_SURSU31617_A0A673TM53_Suricata_suricatta
|     |  |                    +--|
|     |  |                    |  +--SIRT1_MANJA11789_XP_036866792_Manis_javanica
|     |  |                 +--|
|     |  |                 |  |  +--SIRT1_Erinaceus_europaeus_XM_016191849.1
|     |  |                 |  +--|
|     |  |                 |     +--SIRT1_SORAR07806_ENSSARG00000005522_Sorex_araneus
|     |  |              +--|
|     |  |              |  |        +--SIRT1_CALJA05085_ENSCJAG00000019318_Callithrix_jacchus
|     |  |              |  |     +--|
|     |  |              |  |     |  |  +--SIRT1_HUMAN01330_SIR1_HUMAN_Homo_sapiens
|     |  |              |  |     |  +--|
|     |  |              |  |     |     +--SIRT1_MACNE10001_A0A2K6CDP5_Macaca_nemestrina
|     |  |              |  |  +--|
|     |  |              |  |  |  |        +--SIRT1_Cavia_porcellus_XM_023566684.1
|     |  |              |  |  |  |     +--|
|     |  |              |  |  |  |     |  |  +--SIRT1_MOUSE00326_SIR1_MOUSE_Mus_musculus
|     |  |              |  |  |  |     |  +--|
|     |  |              |  |  |  |     |     +--SIRT1_RATNO12227_A0A182DWI7_Rattus_norvegicus
|     |  |              |  |  |  |  +--|
|     |  |              |  |  |  |  |  +--SIRT1_Ictidomys_tridecemlineatus_XM_040273039.1
|     |  |              |  |  |  +--|
|     |  |              |  |  |     +--SIRT1_Dipodomys_ordii_XM_013017208.1
|     |  |              |  +--|
|     |  |              |     +--SIRT1_RABIT06761_G1U0D5_Oryctolagus_cuniculus
|     |  |           +--|
|     |  |           |  |  +--SIRT1_ECHTE04737_ENSETEG00000015512_Echinops_telfairi
|     |  |           |  +--|
|     |  |           |     +--SIRT1_Loxodonta_africana_XM_023546989.1
|     |  |        +--|
|     |  |        |  +--SIRT1_DASNO00242_ENSDNOG00000007191_Dasypus_novemcinctus
|     |  |     +--|
|     |  |     |  |        +--SIRT1_PHACI06199_ENSPCIG00000014645.1_Phascolarctos_cinereus
|     |  |     |  |     +--|
|     |  |     |  |     |  +--SIRT1_VOMUR15393_A0A4X2L8K8_Vombatus_ursinus
|     |  |     |  |  +--|
|     |  |     |  |  |  +--SIRT1_Sarcophilus_harrisii_XM_031956888.1
|     |  |     |  +--|
|     |  |     |     +--SIRT1_Ornithorhynchus_anatinus_XM_029059838.1
|     |  |  +--|
|     |  |  |  |                 +--SIRT1_CALMI02202_A0A4W3GSR4_Callorhinchus_milii
|     |  |  |  |              +--|
|     |  |  |  |              |  |        +--SIRT1_Carcharodon_carcharias_XM_041210202.1
|     |  |  |  |              |  |     +--|
|     |  |  |  |              |  |     |  +--SIRT1_Scyliorhinus_canicula_XM_038821818.1
|     |  |  |  |              |  |  +--|
|     |  |  |  |              |  |  |  |  +--SIRT1_Chiloscyllium_plagiosum_XM_043712270.1
|     |  |  |  |              |  |  |  +--|
|     |  |  |  |              |  |  |     +--SIRT1_Rhincodon_typus_XM_020511459.1
|     |  |  |  |              |  +--|
|     |  |  |  |              |     +--SIRT1_Amblyraja_radiata_XM_033034263.1
|     |  |  |  |           +--|
|     |  |  |  |           |  |     +--SIRT1_DANRE05946_E7F8W3_Danio_rerio
|     |  |  |  |           |  |  +--|
|     |  |  |  |           |  |  |  |     +--SIRT1_ORENI15860_ENSONIG00000010273_Oreochromis_niloticus
|     |  |  |  |           |  |  |  |  +--|
|     |  |  |  |           |  |  |  |  |  +--SIRT1_Takifugu_rubripes_XM_003963728.3
|     |  |  |  |           |  |  |  +--|
|     |  |  |  |           |  |  |     |  +--SIRT1_Oryzias_latipes_XM_004077504.4
|     |  |  |  |           |  |  |     +--|
|     |  |  |  |           |  |  |        +--SIRT1_POEFO14333_A0A087XX78_Poecilia_formosa
|     |  |  |  |           |  +--|
|     |  |  |  |           |     |  +--SIRT1_LEPOC13895_W5MZP3_Lepisosteus_oculatus
|     |  |  |  |           |     +--|
|     |  |  |  |           |        +--SIRT1_Erpetoichthys_calabaricus_XM_028795918.1
|     |  |  |  |        +--|
|     |  |  |  |        |  +--SIRT1_West_African_lungfish_Protopterus_annectens_XM_044056380.1
|     |  |  |  |     +--|
|     |  |  |  |     |  |  +--SIRT1_LATCH03255_H3AHF8_Latimeria_chalumnae
|     |  |  |  |     |  +--|
|     |  |  |  |     |     |    +--------SIRT1_Petromyzon_marinus_filaggrin-like_XM_032978785.1
|     |  |  |  |     |     +----|
|     |  |  |  |     |          |                           +----SIRT3_CALMI44714_A0A4W3HM86_Callorhinchus_milii
|     |  |  |  |     |          |                        +--|
|     |  |  |  |     |          |                        |  |        +--SIRT3-like_Scyliorhinus_canicula_XM_038808032.1
|     |  |  |  |     |          |                        |  |     +--|
|     |  |  |  |     |          |                        |  |     |  +--SIRT3_Carcharodon_carcharias_XM_041197000.1
|     |  |  |  |     |          |                        |  |  +--|
|     |  |  |  |     |          |                        |  |  |  |  +--SIRT3_Chiloscyllium_plagiosum_XM_043705713.1
|     |  |  |  |     |          |                        |  |  |  +--|
|     |  |  |  |     |          |                        |  |  |     +--SIRT3_Rhincodon_typus_XM_020528742.1
|     |  |  |  |     |          |                        |  +--|
|     |  |  |  |     |          |                        |     +-------SIRT3_Amblyraja_radiata_XM_033038599.1
|     |  |  |  |     |          |                     +--|
|     |  |  |  |     |          |                     |  |                    +--SIRT3_GASAC00595_G3PT13_Gasterosteus_aculeatus
|     |  |  |  |     |          |                     |  |                 +--|
|     |  |  |  |     |          |                     |  |                 |  +--SIRT3_Oreochromis_niloticus_XM_013275996.3
|     |  |  |  |     |          |                     |  |              +--|
|     |  |  |  |     |          |                     |  |              |  |  +--SIRT3_Poecilia_formosa_XM_007576827.2
|     |  |  |  |     |          |                     |  |              |  +--|
|     |  |  |  |     |          |                     |  |              |     +--SIRT3_Takifugu_rubripes_XM_011609813.2
|     |  |  |  |     |          |                     |  |           +--|
|     |  |  |  |     |          |                     |  |           |  +--SIRT3_Gadus_morhua_XM_030376087.1
|     |  |  |  |     |          |                     |  |        +--|
|     |  |  |  |     |          |                     |  |        |  +--SIRT3_DANRE39175_ENSDARG00000035819.6_Danio_rerio
|     |  |  |  |     |          |                     |  |     +--|
|     |  |  |  |     |          |                     |  |     |  +--SIRT3_LEPOC10482_W5M391_Lepisosteus_oculatus
|     |  |  |  |     |          |                     |  |  +--|
|     |  |  |  |     |          |                     |  |  |  +----SIRT3_Erpetoichthys_calabaricus_XM_028796144.1
|     |  |  |  |     |          |                     |  +--|
|     |  |  |  |     |          |                     |     |                                         +--SIRT3_HUMAN03534_SIR3_HUMAN_Homo_sapiens
|     |  |  |  |     |          |                     |     |                                      +--|
|     |  |  |  |     |          |                     |     |                                      |  +--SIRT3_CALJA04362_ENSCJAG00000005639_Callithrix_jacchus
|     |  |  |  |     |          |                     |     |                                   +--|
|     |  |  |  |     |          |                     |     |                                   |  +--SIRT3_MACFA08111_ENSMFAG00000041754.1_Macaca_fascicularis
|     |  |  |  |     |          |                     |     |                                +--|
|     |  |  |  |     |          |                     |     |                                |  +--SIRT3_RABIT18645_C6ZII8_Oryctolagus_cuniculus
|     |  |  |  |     |          |                     |     |                             +--|
|     |  |  |  |     |          |                     |     |                             |  +--SIRT3_Tupaia_belangeri_XM_027766673.1
|     |  |  |  |     |          |                     |     |                          +--|
|     |  |  |  |     |          |                     |     |                          |  |           +--SIRT3_BOVIN02950_G5E521_Bos_taurus
|     |  |  |  |     |          |                     |     |                          |  |        +--|
|     |  |  |  |     |          |                     |     |                          |  |        |  +--SIRT3_BALMU09897_ENSBMSG00010009744.1_Balaenoptera_musculus
|     |  |  |  |     |          |                     |     |                          |  |     +--|
|     |  |  |  |     |          |                     |     |                          |  |     |  +--SIRT3_PIGXX16458_A0A480QI11_Sus_scrofa
|     |  |  |  |     |          |                     |     |                          |  |  +--|
|     |  |  |  |     |          |                     |     |                          |  |  |  |  +--SIRT3_CANLF05591_F6Y2M8_Canis_lupus_familiaris
|     |  |  |  |     |          |                     |     |                          |  |  |  +--|
|     |  |  |  |     |          |                     |     |                          |  |  |     |  +--SIRT3_FELCA12499_ENSFCAG00000002668_Felis_catus
|     |  |  |  |     |          |                     |     |                          |  |  |     +--|
|     |  |  |  |     |          |                     |     |                          |  |  |        +--SIRT3_SURSU34923_A0A673TCM6_Suricata_suricatta
|     |  |  |  |     |          |                     |     |                          |  +--|
|     |  |  |  |     |          |                     |     |                          |     |  +--SIRT3_PTEVA04051_ENSPVAG00000017703_Pteropus_vampyrus
|     |  |  |  |     |          |                     |     |                          |     +--|
|     |  |  |  |     |          |                     |     |                          |        |  +--SIRT3_Equus_caballus_XM_023654877.1
|     |  |  |  |     |          |                     |     |                          |        +--|
|     |  |  |  |     |          |                     |     |                          |           +--SIRT3_MANJA50844_XP_036858345_Manis_javanica
|     |  |  |  |     |          |                     |     |                       +--|
|     |  |  |  |     |          |                     |     |                       |  |  +--SIRT3_CAVAP18143_ENSCAPG00000014228.1_Cavia_aperea
|     |  |  |  |     |          |                     |     |                       |  +--|
|     |  |  |  |     |          |                     |     |                       |     +--SIRT3_Ictidomys_tridecemlineatus_XM_005341607.4
|     |  |  |  |     |          |                     |     |                    +--|
|     |  |  |  |     |          |                     |     |                    |  |  +--SIRT3_MOUSE56805_SIR3_MOUSE_Mus_musculus
|     |  |  |  |     |          |                     |     |                    |  +--|
|     |  |  |  |     |          |                     |     |                    |     +--SIRT3_RATNO02156_C6ZII9_Rattus_norvegicus
|     |  |  |  |     |          |                     |     |                 +--|
|     |  |  |  |     |          |                     |     |                 |  |  +--SIRT3_LOXAF00900_G3U401_Loxodonta_africana
|     |  |  |  |     |          |                     |     |                 |  +--|
|     |  |  |  |     |          |                     |     |                 |     +--SIRT3_Dasypus_novemcinctus_XM_023583621.1
|     |  |  |  |     |          |                     |     |              +--|
|     |  |  |  |     |          |                     |     |              |  |  +--SIRT3_Sarcophilus_harrisii_XM_031942757.1
|     |  |  |  |     |          |                     |     |              |  +--|
|     |  |  |  |     |          |                     |     |              |     |  +--SIRT3_PHACI31658_A0A6P5JNH1_Phascolarctos_cinereus
|     |  |  |  |     |          |                     |     |              |     +--|
|     |  |  |  |     |          |                     |     |              |        +--SIRT3_VOMUR29351_A0A4X2K1X1_Vombatus_ursinus
|     |  |  |  |     |          |                     |     |           +--|
|     |  |  |  |     |          |                     |     |           |  |                     +--SIRT3_Xenopus_tropicalis_XM_012962148.3
|     |  |  |  |     |          |                     |     |           |  |                +----|
|     |  |  |  |     |          |                     |     |           |  |                |    +--SIRT3_XENLA19366_A0A1L8GDN0_Xenopus_laevis
|     |  |  |  |     |          |                     |     |           |  |             +--|
|     |  |  |  |     |          |                     |     |           |  |             |  |  +--SIRT3_Nanorana_parkeri_XM_018567219.1
|     |  |  |  |     |          |                     |     |           |  |             |  +--|
|     |  |  |  |     |          |                     |     |           |  |             |     +--SIRT3_Rana_temporaria_XM_040328438.1
|     |  |  |  |     |          |                     |     |           |  |  +----------|
|     |  |  |  |     |          |                     |     |           |  |  |          +--SIRT3_Bufo_bufo_XM_040409401.1
|     |  |  |  |     |          |                     |     |           |  +--|
|     |  |  |  |     |          |                     |     |           |     +--SIRT3_Ornithorhynchus_anatinus_XM_029060503.2
|     |  |  |  |     |          |                     |     |        +--|
|     |  |  |  |     |          |                     |     |        |  |  +--SIRT3_Microcaecilia_unicolor_XM_030201197.1
|     |  |  |  |     |          |                     |     |        |  +--|
|     |  |  |  |     |          |                     |     |        |     +--SIRT3_Rhinatrema_bivittatum_XM_029582817.1
|     |  |  |  |     |          |                     |     |     +--|
|     |  |  |  |     |          |                     |     |     |  |        +--SIRT3_ANAPP17745_U3J7C6_Anas_platyrhynchos_platyrhynchos
|     |  |  |  |     |          |                     |     |     |  |     +--|
|     |  |  |  |     |          |                     |     |     |  |     |  |  +--SIRT3_CHICK21600_A0A1D5NYI2_Gallus_gallus
|     |  |  |  |     |          |                     |     |     |  |     |  +--|
|     |  |  |  |     |          |                     |     |     |  |     |     +--SIRT3_PHACC16976_A0A669QJP1_Phasianus_colchicus
|     |  |  |  |     |          |                     |     |     |  |  +--|
|     |  |  |  |     |          |                     |     |     |  |  |  |  +--SIRT3_FICAL12686_U3KAA2_Ficedula_albicollis
|     |  |  |  |     |          |                     |     |     |  |  |  +--|
|     |  |  |  |     |          |                     |     |     |  |  |     +--SIRT3_TAEGU10236_ENSTGUG00000006886_Taeniopygia_guttata
|     |  |  |  |     |          |                     |     |     |  +--|
|     |  |  |  |     |          |                     |     |     |     |     +--SIRT3_CHRPI28225_ENSCPBG00000022149.1_Chrysemys_picta_bellii
|     |  |  |  |     |          |                     |     |     |     |  +--|
|     |  |  |  |     |          |                     |     |     |     |  |  |     +--SIRT3_PODMU07057_A0A670HQC6_Podarcis_muralis
|     |  |  |  |     |          |                     |     |     |     |  |  |  +--|
|     |  |  |  |     |          |                     |     |     |     |  |  |  |  +--SIRT3_PSETE12110_A0A670Y173_Pseudonaja_textilis
|     |  |  |  |     |          |                     |     |     |     |  |  +--|
|     |  |  |  |     |          |                     |     |     |     |  |     +--SIRT3_ANOCA00588_G1KU17_Anolis_carolinensis
|     |  |  |  |     |          |                     |     |     |     +--|
|     |  |  |  |     |          |                     |     |     |        |     +--SIRT3_Alligator_mississippiensis_XM_006274883.3
|     |  |  |  |     |          |                     |     |     |        |  +--|
|     |  |  |  |     |          |                     |     |     |        |  |  +--SIRT3_Alligator_sinensis_XM_006033338.3
|     |  |  |  |     |          |                     |     |     |        +--|
|     |  |  |  |     |          |                     |     |     |           |  +--SIRT3_Gavialis_gangeticus_XM_019519864.1
|     |  |  |  |     |          |                     |     |     |           +--|
|     |  |  |  |     |          |                     |     |     |              +--SIRT3_Crocodylus_porosus_XM_019547230.1
|     |  |  |  |     |          |                     |     |  +--|
|     |  |  |  |     |          |                     |     |  |  +-----SIRT3_West_African_lungfish_Protopterus_annectens_XM_044082822.1
|     |  |  |  |     |          |                     |     +--|
|     |  |  |  |     |          |                     |        +----SIRT3_LATCH03716_H3AAK4_Latimeria_chalumnae
|     |  |  |  |     |          +---------------------|
|     |  |  |  |     |                                |  +-----------SIRT3_EPTBU18760_ENSEBUG00000003693.1_Eptatretus_burgeri
|     |  |  |  |     |                                +--|
|     |  |  |  |     |                                   |  +-------SIRT3_Petromyzon_marinus_XM_032949194.1
|     |  |  |  |     |                                   +--|
|     |  |  |  |     |                                      |                      +--SIRT3-like_Xenopus_laevis_NM_001096098.1
|     |  |  |  |     |                                      |               +------|
|     |  |  |  |     |                                      |               |      +--SIRT3-like_Xenopus_tropicalis_XM_031897650.1
|     |  |  |  |     |                                      |            +--|
|     |  |  |  |     |                                      |            |  |     +--SIRT3-like_Nanorana_parkeri_XM_018557664.1
|     |  |  |  |     |                                      |            |  |  +--|
|     |  |  |  |     |                                      |            |  |  |  +--SIRT3-like_Rana_temporaria_XM_040345208.1
|     |  |  |  |     |                                      |            |  +--|
|     |  |  |  |     |                                      |            |     +--SIRT3-like_Bufo_bufo_XM_040439569.1
|     |  |  |  |     |                                      |         +--|
|     |  |  |  |     |                                      |         |  +--------SIRT3_like_West_African_lungfish_Protopterus_annectens_XM_044088148.1
|     |  |  |  |     |                                      |      +--|
|     |  |  |  |     |                                      |      |  |                        +--SIRT3-like_Poecilia_formosa_ENSPFOG00000016528
|     |  |  |  |     |                                      |      |  |                     +--|
|     |  |  |  |     |                                      |      |  |                     |  +--SIRT3-like_Xiphophorus_maculatus_ENSXMAG00000001675
|     |  |  |  |     |                                      |      |  |                  +--|
|     |  |  |  |     |                                      |      |  |                  |  +--SIRT3-like_Oryzias_latipes_ENSORLG00000030051
|     |  |  |  |     |                                      |      |  |               +--|
|     |  |  |  |     |                                      |      |  |               |  |  +--SIRT3-like_Oreochromis_aureus_ENSOABG00000011452
|     |  |  |  |     |                                      |      |  |               |  +--|
|     |  |  |  |     |                                      |      |  |               |     +--SIRT3-like_Gasterosteus_aculeatus_ENSGACG00000011838
|     |  |  |  |     |                                      |      |  |            +--|
|     |  |  |  |     |                                      |      |  |            |  +--SIRT3-like_Fugu_ENSTRUG00000009380
|     |  |  |  |     |                                      |      |  |         +--|
|     |  |  |  |     |                                      |      |  |         |  +---SIRT3-like_Gadus_morua_ENSGMOG00000004353
|     |  |  |  |     |                                      |      |  |     +---|
|     |  |  |  |     |                                      |      |  |     |   |  +--SIRT3-like_Astyanaxmexicanus_ENSAMXG00000020124
|     |  |  |  |     |                                      |      |  |     |   +--|
|     |  |  |  |     |                                      |      |  |     |      +--SIRT3-like_Danio_rerio_ENSDARG00000062893
|     |  |  |  |     |                                      |      |  |  +--|
|     |  |  |  |     |                                      |      |  |  |  +--SIRT3-like_Lepisosteusoculatus_XM_015351860.1
|     |  |  |  |     |                                      |      |  +--|
|     |  |  |  |     |                                      |      |     +------SIRT3-like_Erpetoichthys_calabaricus_XM_028816076.1
|     |  |  |  |     |                                      |   +--|
|     |  |  |  |     |                                      |   |  +----SIRT3-like_Latimeria_chalumnae_ENSLACG00000017633
|     |  |  |  |     |                                      +---|
|     |  |  |  |     |                                          |  +----SIRT3-like_Elephant_fish_NW_024704746.1_c30371848-30354578
|     |  |  |  |     |                                          +--|
|     |  |  |  |     |                                             +--------SIRT3-like_Carcharodon_carcharias_XM_041215676.1_LOC557125
|     |  |  |  |  +--|
|     |  |  |  |  |  |        +--SIRT1_XENLA29634_A0A1L8FJP8_Xenopus_laevis
|     |  |  |  |  |  |     +--|
|     |  |  |  |  |  |     |  +--SIRT1_XENLA31850_A0A1L8FEV8_Xenopus_laevis
|     |  |  |  |  |  |  +--|
|     |  |  |  |  |  |  |  +--SIRT1_XENTR08910_ENSXETG00000023588_Xenopus_tropicalis
|     |  |  |  |  |  +--|
|     |  |  |  |  |     |     +--SIRT1_Nanorana_parkeri_XM_018573518.1
|     |  |  |  |  |     |  +--|
|     |  |  |  |  |     |  |  +--SIRT1_Rana_temporaria_XM_040362158.1
|     |  |  |  |  |     +--|
|     |  |  |  |  |        +--SIRT1_Bufo_bufo_XM_040438305.1
|     |  |  |  +--|
|     |  |  |     |     +--SIRT1_Microcaecilia_unicolor_XM_030203130.1
|     |  |  |     |  +--|
|     |  |  |     |  |  +--SIRT1_Geotrypetes_seraphini_XM_033941657.1
|     |  |  |     +--|
|     |  |  |        +--SIRT1_Rhinatrema_bivittatum_XM_029609704.1
|     |  +--|
|     |     |     +--SIRT1_CHEAB04289_ENSCABG00000022942.1_Chelonoidis_abingdonii
|     |     |  +--|
|     |     |  |  +--SIRT1_CHRPI04679_ENSCPBG00000019531.1_Chrysemys_picta_bellii
|     |     +--|
|     |        +--SIRT1_Pelodiscus_sinensis_XM_006125276.3
|  +--|
|  |  |     +--SIRT1_Alligator_mississippiensis_XM_019484400.1
|  |  |  +--|
|  |  |  |  +**SIRT1_Alligator_sinensis_XM_006024809.3
|  |  +--|
|  |     |  +--SIRT1_Gavialis_gangeticus_XM_019507879.1
|  |     +--|
|  |        +--SIRT1_Crocodylus_porosus_XM_019538640.1
+--|
|  +--SIRT1_Gallus_gallus_NM_001004767.2
|
|  +--SIRT1_Ficedula_albicollis_XM_005047913.1
+--|
   +--SIRT1_Taeniopygia_guttata_XM_041717094.1

Tree in newick format:

(SIRT1_ANAPP23028_U3I9B7_Anas_platyrhynchos_platyrhynchos:0.0365779074,(((((SIRT1_ANOCA17241_ENSACAG00000010558_Anolis_carolinensis:0.1325773487,(SIRT1_PODMU36783_A0A670IVI5_Podarcis_muralis:0.0729923264,SIRT1_PSETE18241_A0A670YFV4_Pseudonaja_textilis:0.1676829283):0.0132246711):0.0877242139,SIRT1_SPHPU13742_ENSSPUG00000001982.1_Sphenodon_punctatus:0.1214583871):0.0547930745,(((((((((((((SIRT1_BALMU15616_ENSBMSG00010012923.1_Balaenoptera_musculus:0.0191757001,SIRT1_BOVIN22421_F1MQB8_Bos_taurus:0.0284088248):0.0025499104,SIRT1_PIGXX07692_A0A4X1TRZ2_Sus_scrofa:0.0308650949):0.0105658026,(SIRT1_Myotis_lucifugus_XM_006089727.3:0.0616339778,SIRT1_PTEVA07651_ENSPVAG00000007129_Pteropus_vampyrus:0.0414881757):0.0026821163):0.0015245270,SIRT1_Equus_caballus_XM_023643979.1:0.0344060523):0.0013151984,(SIRT1_CANLF14607_E2RE73_Canis_lupus_familiaris:0.0202147793,(SIRT1_Felis_catus_NM_001290246.1:0.0111876616,SIRT1_SURSU31617_A0A673TM53_Suricata_suricatta:0.0139925485):0.0047350128):0.0166657768):0.0021693130,SIRT1_MANJA11789_XP_036866792_Manis_javanica:0.0625788551):0.0019514420,(SIRT1_Erinaceus_europaeus_XM_016191849.1:0.0734441224,SIRT1_SORAR07806_ENSSARG00000005522_Sorex_araneus:0.0668158955):0.0312122733):0.0079847608,(((SIRT1_CALJA05085_ENSCJAG00000019318_Callithrix_jacchus:0.0253966905,(SIRT1_HUMAN01330_SIR1_HUMAN_Homo_sapiens:0.0121980861,SIRT1_MACNE10001_A0A2K6CDP5_Macaca_nemestrina:0.0107440333):0.0084163840):0.0213130629,(((SIRT1_Cavia_porcellus_XM_023566684.1:0.0486651451,(SIRT1_MOUSE00326_SIR1_MOUSE_Mus_musculus:0.0437848998,SIRT1_RATNO12227_A0A182DWI7_Rattus_norvegicus:0.1630091996):0.0440177708):0.0047645778,SIRT1_Ictidomys_tridecemlineatus_XM_040273039.1:0.0742304958):0.0076988872,SIRT1_Dipodomys_ordii_XM_013017208.1:0.0747166569):0.0106005774):0.0071837385,SIRT1_RABIT06761_G1U0D5_Oryctolagus_cuniculus:0.0553151174):0.0053477501):0.0050794166,(SIRT1_ECHTE04737_ENSETEG00000015512_Echinops_telfairi:0.0617028429,SIRT1_Loxodonta_africana_XM_023546989.1:0.0556177271):0.0034210892):0.0038468442,SIRT1_DASNO00242_ENSDNOG00000007191_Dasypus_novemcinctus:0.0661262803):0.0857596213,(((SIRT1_PHACI06199_ENSPCIG00000014645.1_Phascolarctos_cinereus:0.0090168014,SIRT1_VOMUR15393_A0A4X2L8K8_Vombatus_ursinus:0.0238275890):0.0205493206,SIRT1_Sarcophilus_harrisii_XM_031956888.1:0.0494995767):0.1172599857,SIRT1_Ornithorhynchus_anatinus_XM_029059838.1:0.1922306305):0.0381320504):0.0939729684,((((((SIRT1_CALMI02202_A0A4W3GSR4_Callorhinchus_milii:0.2028113725,(((SIRT1_Carcharodon_carcharias_XM_041210202.1:0.0533458755,SIRT1_Scyliorhinus_canicula_XM_038821818.1:0.0594073918):0.0257957182,(SIRT1_Chiloscyllium_plagiosum_XM_043712270.1:0.0515973581,SIRT1_Rhincodon_typus_XM_020511459.1:0.0451763264):0.0900710508):0.0316448798,SIRT1_Amblyraja_radiata_XM_033034263.1:0.1823974561):0.0697517054):0.4488824919,((SIRT1_DANRE05946_E7F8W3_Danio_rerio:0.5188637985,((SIRT1_ORENI15860_ENSONIG00000010273_Oreochromis_niloticus:0.0978975002,SIRT1_Takifugu_rubripes_XM_003963728.3:0.1962131107):0.0290667453,(SIRT1_Oryzias_latipes_XM_004077504.4:0.2188127232,SIRT1_POEFO14333_A0A087XX78_Poecilia_formosa:0.1936203211):0.0321542117):0.2727478878):0.2398492110,(SIRT1_LEPOC13895_W5MZP3_Lepisosteus_oculatus:0.2188849228,SIRT1_Erpetoichthys_calabaricus_XM_028795918.1:0.3425412502):0.0740732250):0.1951300488):0.0293854336,SIRT1_West_African_lungfish_Protopterus_annectens_XM_044056380.1:0.3973872290):0.0341727642,(SIRT1_LATCH03255_H3AHF8_Latimeria_chalumnae:0.5593922738,(SIRT1_Petromyzon_marinus_filaggrin-like_XM_032978785.1:1.6429086736,(((SIRT3_CALMI44714_A0A4W3HM86_Callorhinchus_milii:0.9042444261,(((SIRT3-like_Scyliorhinus_canicula_XM_038808032.1:0.1796756951,SIRT3_Carcharodon_carcharias_XM_041197000.1:0.1138004224):0.1111833622,(SIRT3_Chiloscyllium_plagiosum_XM_043705713.1:0.1143375779,SIRT3_Rhincodon_typus_XM_020528742.1:0.0667844783):0.1012532131):0.5424269112,SIRT3_Amblyraja_radiata_XM_033038599.1:1.5149176510):0.2737034333):0.5405728965,(((((((SIRT3_GASAC00595_G3PT13_Gasterosteus_aculeatus:0.5807243180,SIRT3_Oreochromis_niloticus_XM_013275996.3:0.2453058760):0.0521376440,(SIRT3_Poecilia_formosa_XM_007576827.2:0.4437653791,SIRT3_Takifugu_rubripes_XM_011609813.2:0.3420006636):0.1200325729):0.0893497480,SIRT3_Gadus_morhua_XM_030376087.1:0.5986803778):0.2704196371,SIRT3_DANRE39175_ENSDARG00000035819.6_Danio_rerio:0.4648821468):0.3925183605,SIRT3_LEPOC10482_W5M391_Lepisosteus_oculatus:0.5715589264):0.1746131147,SIRT3_Erpetoichthys_calabaricus_XM_028796144.1:0.9724805887):0.2877061644,((((((((((((((SIRT3_HUMAN03534_SIR3_HUMAN_Homo_sapiens:0.0458840613,SIRT3_CALJA04362_ENSCJAG00000005639_Callithrix_jacchus:0.1530681194):0.0081518372,SIRT3_MACFA08111_ENSMFAG00000041754.1_Macaca_fascicularis:0.0699582438):0.2035720574,SIRT3_RABIT18645_C6ZII8_Oryctolagus_cuniculus:0.3895065455):0.0067702758,SIRT3_Tupaia_belangeri_XM_027766673.1:0.3137395604):0.0556465551,((((SIRT3_BOVIN02950_G5E521_Bos_taurus:0.3596708885,SIRT3_BALMU09897_ENSBMSG00010009744.1_Balaenoptera_musculus:0.1777859687):0.0467202987,SIRT3_PIGXX16458_A0A480QI11_Sus_scrofa:0.1951960959):0.2462613036,(SIRT3_CANLF05591_F6Y2M8_Canis_lupus_familiaris:0.2456981830,(SIRT3_FELCA12499_ENSFCAG00000002668_Felis_catus:0.2249235618,SIRT3_SURSU34923_A0A673TCM6_Suricata_suricatta:0.0739295475):0.0855487930):0.2598622697):0.0342864982,(SIRT3_PTEVA04051_ENSPVAG00000017703_Pteropus_vampyrus:0.4377304156,(SIRT3_Equus_caballus_XM_023654877.1:0.2084824841,SIRT3_MANJA50844_XP_036858345_Manis_javanica:0.4501505199):0.0137960345):0.0590848246):0.0400495583):0.0423475400,(SIRT3_CAVAP18143_ENSCAPG00000014228.1_Cavia_aperea:0.3570527995,SIRT3_Ictidomys_tridecemlineatus_XM_005341607.4:0.1381678668):0.1217104736):0.0455853510,(SIRT3_MOUSE56805_SIR3_MOUSE_Mus_musculus:0.1155671741,SIRT3_RATNO02156_C6ZII9_Rattus_norvegicus:0.1084855915):0.3295387723):0.0578598509,(SIRT3_LOXAF00900_G3U401_Loxodonta_africana:0.4771860033,SIRT3_Dasypus_novemcinctus_XM_023583621.1:0.4925605469):0.1016882022):0.2367402238,(SIRT3_Sarcophilus_harrisii_XM_031942757.1:0.3772309407,(SIRT3_PHACI31658_A0A6P5JNH1_Phascolarctos_cinereus:0.0368344250,SIRT3_VOMUR29351_A0A4X2K1X1_Vombatus_ursinus:0.0920344930):0.3231962176):0.4618943814):0.1517500556,((((SIRT3_Xenopus_tropicalis_XM_012962148.3:0.2007172626,SIRT3_XENLA19366_A0A1L8GDN0_Xenopus_laevis:0.1245723319):0.8936158728,(SIRT3_Nanorana_parkeri_XM_018567219.1:0.1655721295,SIRT3_Rana_temporaria_XM_040328438.1:0.2790666702):0.3174511557):0.2058986109,SIRT3_Bufo_bufo_XM_040409401.1:0.2315956767):1.9816195214,SIRT3_Ornithorhynchus_anatinus_XM_029060503.2:0.4153696272):0.3020973656):0.1333087461,(SIRT3_Microcaecilia_unicolor_XM_030201197.1:0.3426231723,SIRT3_Rhinatrema_bivittatum_XM_029582817.1:0.2356784461):0.4180783063):0.1047164248,(((SIRT3_ANAPP17745_U3J7C6_Anas_platyrhynchos_platyrhynchos:0.1924896077,(SIRT3_CHICK21600_A0A1D5NYI2_Gallus_gallus:0.0150831296,SIRT3_PHACC16976_A0A669QJP1_Phasianus_colchicus:0.0686019303):0.1225188664):0.0578548549,(SIRT3_FICAL12686_U3KAA2_Ficedula_albicollis:0.1339279471,SIRT3_TAEGU10236_ENSTGUG00000006886_Taeniopygia_guttata:0.0771053569):0.1767491128):0.3067025691,((SIRT3_CHRPI28225_ENSCPBG00000022149.1_Chrysemys_picta_bellii:0.2496972924,((SIRT3_PODMU07057_A0A670HQC6_Podarcis_muralis:0.3212549335,SIRT3_PSETE12110_A0A670Y173_Pseudonaja_textilis:0.4466007018):0.0502879743,SIRT3_ANOCA00588_G1KU17_Anolis_carolinensis:0.3613879986):0.2991112307):0.0594343196,((SIRT3_Alligator_mississippiensis_XM_006274883.3:0.0285519509,SIRT3_Alligator_sinensis_XM_006033338.3:0.0261828677):0.0144330973,(SIRT3_Gavialis_gangeticus_XM_019519864.1:0.0271222810,SIRT3_Crocodylus_porosus_XM_019547230.1:0.0265230229):0.0343814849):0.2774418868):0.1321264371):0.1369784993):0.3318317058,SIRT3_West_African_lungfish_Protopterus_annectens_XM_044082822.1:1.1796688256):0.1902711555,SIRT3_LATCH03716_H3AAK4_Latimeria_chalumnae:0.9828833574):0.1073623031):0.1606146862):0.3593109374,(SIRT3_EPTBU18760_ENSEBUG00000003693.1_Eptatretus_burgeri:2.0513530929,(SIRT3_Petromyzon_marinus_XM_032949194.1:1.4821480152,((((((SIRT3-like_Xenopus_laevis_NM_001096098.1:0.2328219899,SIRT3-like_Xenopus_tropicalis_XM_031897650.1:0.1613747369):1.2565977198,((SIRT3-like_Nanorana_parkeri_XM_018557664.1:0.3087718912,SIRT3-like_Rana_temporaria_XM_040345208.1:0.3115550662):0.3643132465,SIRT3-like_Bufo_bufo_XM_040439569.1:0.4372704396):0.3223607285):0.2986626547,SIRT3_like_West_African_lungfish_Protopterus_annectens_XM_044088148.1:1.6321346565):0.1686329704,((((((((SIRT3-like_Poecilia_formosa_ENSPFOG00000016528:0.0359420493,SIRT3-like_Xiphophorus_maculatus_ENSXMAG00000001675:0.0240309018):0.3175741985,SIRT3-like_Oryzias_latipes_ENSORLG00000030051:0.5091829633):0.0769067524,(SIRT3-like_Oreochromis_aureus_ENSOABG00000011452:0.1229287207,SIRT3-like_Gasterosteus_aculeatus_ENSGACG00000011838:0.4332681416):0.0396353286):0.0472487935,SIRT3-like_Fugu_ENSTRUG00000009380:0.3294243707):0.1280605564,SIRT3-like_Gadus_morua_ENSGMOG00000004353:0.8287353388):0.0899163838,(SIRT3-like_Astyanaxmexicanus_ENSAMXG00000020124:0.2377107730,SIRT3-like_Danio_rerio_ENSDARG00000062893:0.3485151798):0.3210522581):0.8242017543,SIRT3-like_Lepisosteusoculatus_XM_015351860.1:0.5978719313):0.3044936739,SIRT3-like_Erpetoichthys_calabaricus_XM_028816076.1:1.3440042871):0.2417576405):0.1378620258,SIRT3-like_Latimeria_chalumnae_ENSLACG00000017633:0.9546750283):0.0869041669,(SIRT3-like_Elephant_fish_NW_024704746.1_c30371848-30354578:0.8891638128,SIRT3-like_Carcharodon_carcharias_XM_041215676.1_LOC557125:1.5708702657):0.3040624258):0.7887976785):0.2417115045):0.2317392206):3.7597966842):0.9054121858):0.0145427565):0.1292042720,(((SIRT1_XENLA29634_A0A1L8FJP8_Xenopus_laevis:0.0280510245,SIRT1_XENLA31850_A0A1L8FEV8_Xenopus_laevis:0.0595140179):0.0127436294,SIRT1_XENTR08910_ENSXETG00000023588_Xenopus_tropicalis:0.0322921130):0.2452763423,((SIRT1_Nanorana_parkeri_XM_018573518.1:0.0482490593,SIRT1_Rana_temporaria_XM_040362158.1:0.0625614089):0.1439588722,SIRT1_Bufo_bufo_XM_040438305.1:0.1429312899):0.1481988242):0.3098914732):0.0423836153,((SIRT1_Microcaecilia_unicolor_XM_030203130.1:0.0442696804,SIRT1_Geotrypetes_seraphini_XM_033941657.1:0.0527806420):0.0438957143,SIRT1_Rhinatrema_bivittatum_XM_029609704.1:0.0398755490):0.1638950455):0.0747591942):0.1055125491,((SIRT1_CHEAB04289_ENSCABG00000022942.1_Chelonoidis_abingdonii:0.0296222613,SIRT1_CHRPI04679_ENSCPBG00000019531.1_Chrysemys_picta_bellii:0.0192822987):0.0264986157,SIRT1_Pelodiscus_sinensis_XM_006125276.3:0.0860892789):0.0332199963):0.0202113807):0.0126028461,((SIRT1_Alligator_mississippiensis_XM_019484400.1:0.0226681550,SIRT1_Alligator_sinensis_XM_006024809.3:0.0000010000):0.0026825753,(SIRT1_Gavialis_gangeticus_XM_019507879.1:0.0040572143,SIRT1_Crocodylus_porosus_XM_019538640.1:0.0136006234):0.0050506711):0.0622944543):0.0842248226,SIRT1_Gallus_gallus_NM_001004767.2:0.0552794218):0.0115333324,(SIRT1_Ficedula_albicollis_XM_005047913.1:0.0480537670,SIRT1_Taeniopygia_guttata_XM_041717094.1:0.0215006610):0.0770092666);

TIME STAMP
----------

Date and time: Sat Sep  9 16:57:44 2023
Total CPU time used: 12536 seconds (3h:28m:55s)
Total wall-clock time used: 3224.83 seconds (0h:53m:44s)

